noisereduce==1.1.0
librosa==0.7.2
matplotlib==3.2.2
argparse==1.4.0
numpy==1.19.0
scikit-learn==0.23.1
joblib==0.16.0
jiwer==2.1.0