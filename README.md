# ASR

An Automatic Speech Recognition system for converting spoken digits to text.

<img src="Flow.png"
     alt="System diagram"
     style="width: 100%;" />

# Requirements

* python3
* pip

Run the command

`pip3 install -r requirements.txt`

# Usage

Example

`python3 main.py kalimera.wav`