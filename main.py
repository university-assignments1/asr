import librosa
import librosa.display # need to be imported explicitly
import noisereduce as nr
import matplotlib.pyplot as plt
import numpy as np
import argparse

from free_spoken_digit_dataset_master.utils import trimmer

from sklearn import svm

from os import listdir
from os.path import isfile, join

from joblib import dump, load

import jiwer

def argparser():
	global args
	global parser

	parser = argparse.ArgumentParser(description='ASR system for digits.')
	parser.add_argument('filename', type=str, help='Input audio file')
	parser.add_argument('-v', '--verbose', help='Enable verbose', default=False, dest='verbose', action='store_true')
	parser.add_argument('-d', '--dataset', help='Set the dataset diretory', dest='dataset',
		default='free_spoken_digit_dataset_master/training/')
	parser.add_argument('-t', '--testing', help='Set the file with the corresponding labels of the input wav', dest='labels_file')
	parser.add_argument('-s', '--exclude_speakers', nargs='+', help='Exclude speakers from training dataset (give list of names separated by space)', dest='exspeak',
		default=None)

	args = parser.parse_args()

# Defined file format on Jakobovski/free-spoken-digit-dataset
get_label = lambda x: x.split('_')[0]

args=None
parser=None

argparser()


# --- PRE PROCESSING ---	

# Returns a list of denoised audio arrays.
def preprocessing(audio_clip, sample_rate):

	#Find the max absolute aplitude of the first 0.5 seconds in the signal
	max_A = np.amax(audio_clip[:sample_rate//2])
	if max_A == 0:#if there is zero noise
	    max_A = 150 #default value of noise_threshold

	#Get the silent zones from the given signal
	silence_zones = trimmer.get_silence_zones(audio_clip,min_silence_duration=0.5,noise_threshold= max_A,
		sample_rate_hz=sample_rate)

	#Find max aplitude of the noise parts of the signal
	max_A = 0
	for silence_zone in silence_zones:
	    for point in range(silence_zone[0],silence_zone[1]):
	        if(max_A < abs(audio_clip[point])):
	            max_A = abs(audio_clip[point])

	noise_sample = audio_clip[silence_zones[0][0]:silence_zones[0][1]]
	reduced_noise = nr.reduce_noise(audio_clip, noise_sample)
	if args.verbose:
		librosa.output.write_wav(args.filename.split('.')[0] + "_denoised.wav",reduced_noise/2**15, sample_rate)

	#Trim the silence and the beggining and end of the signal
	reduced_noise = trimmer.trim_silence(reduced_noise, max_A)

	#Get silent zones of processed signal
	silence_zones = trimmer.get_silence_zones(reduced_noise,min_silence_duration=0.5,noise_threshold=max_A,
	sample_rate_hz=sample_rate)
	if (len(silence_zones) <= 1):
		silence_zones = trimmer.get_silence_zones(reduced_noise, sample_rate_hz=sample_rate)

	# Split spoken digits
	splitted_audio = trimmer.split_multiple_recordings(reduced_noise, silence_zones)
	splitted_audio = [x/2**15 for x in splitted_audio if (x != [] and len(x)>1600)] #1600=200ms

	if args.verbose:
		for idx, recording in enumerate(splitted_audio):
			new_file_path = args.filename.split('.')[0] + '_' + str(idx) + ".wav"
			print("Writing " + new_file_path)
			librosa.output.write_wav(new_file_path, recording, sample_rate, norm=True)

		fig3 = plt.figure(constrained_layout=True)
		gs = fig3.add_gridspec(3, len(splitted_audio))
		
		plt.subplot(gs[0,:])
		librosa.display.waveplot(audio_clip)
		plt.title('Original')

		plt.subplot(gs[1,:])
		librosa.display.waveplot(reduced_noise)
		plt.title('Reduced noise')
		
		for i, s in enumerate(splitted_audio):
			plt.subplot(gs[2,i])
			librosa.display.waveplot(s)

		# Hide all values from axes
		for ax in plt.gcf().get_axes():
			ax.xaxis.set_visible(False)
			ax.yaxis.set_visible(False)

		plt.show()

	return splitted_audio

# Extract the mfcc features from f_audio
def extract_features(f_audio, f_sr):
	mfcc_vector = librosa.feature.mfcc(f_audio, sr=f_sr, n_mfcc=80)
	mfcc_vector = np.mean(mfcc_vector.T, axis=0)
	return mfcc_vector

# --- TRAINING ---
clf = None

# Train the SVM model using the given training dataset.
def training():
	global clf

	print("=== Training ===")

	if isfile('model.joblib'):
		print("Found existing model (model.joblib)")
		clf = load('model.joblib') 
	else:
		print("Creating new model using the dataset " + args.dataset)
		# Load sounds from dataset & create X (vectors) and y (labels)
		X = []
		y = []

		if args.exspeak:
			print("Train model without the speaker(s) " + ' '.join(args.exspeak))

		for f in listdir(args.dataset):
			if not isfile(join(args.dataset, f)):
				continue

			if args.exspeak:
				if f.split('_')[1] in args.exspeak:
					continue

			f_audio, f_sr = librosa.load(args.dataset+f, sr=8000)
			mfcc_vector = extract_features(f_audio, f_sr)
			
			X.append(mfcc_vector)
			y.append(get_label(f))

		# SVM fitting
		clf = svm.SVC()
		clf.fit(X, y)

		print("Saving the trained model")
		dump(clf, 'model.joblib')

# Test the SVM model using the given testing dataset.
def testing(recognised):

	count_svm = 0
	count_total = 0

	test_folder = 'free_spoken_digit_dataset_master/testing/'

	for f in listdir(test_folder):
		if not isfile(join(test_folder, f)):
			continue

		temp_file = test_folder + f
		f_audio, f_sr = librosa.load(temp_file, sr=8000)
		mfcc_vector = extract_features(f_audio, f_sr)

		number = clf.predict([mfcc_vector])[0]

		if number == get_label(f):
			count_svm += 1

		count_total += 1

	# Statistics from the dataset
	print("=== Performance evaluation ===")
	print("Number of successes {} of {} ({:.2f}%)".format(count_svm, count_total, count_svm/count_total*100))

	# Calculate WER
	if isfile(args.labels_file):
		file = open(args.labels_file, 'r')
		ground_truth = ' '.join(file.readline())
		hypothesis = ' '.join(recognised)

		error = jiwer.wer(ground_truth, hypothesis)
		print("WER: {:.2f}%".format(error*100))
	else:
		print("Couldn't find " + args.labels_file + " file")

# Recognise using a specific model
def recognise(audio_list):

	ret = []

	for a in audio_list:

		mfcc_vector = extract_features(a, 8000)
		ret.append(clf.predict([mfcc_vector])[0])

	return ret

audio_clip, sample_rate = librosa.load(args.filename, sr=8000)
splitted_audio = preprocessing(audio_clip * 2**15, sample_rate)

training()

recognised = recognise(splitted_audio)
print("Recognised digits from file " + args.filename)
print(' '.join(recognised))

# --- PERFORMANCE EVALUATION  ---

if args.labels_file:
	testing(recognised)
